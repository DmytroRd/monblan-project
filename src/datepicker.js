document.addEventListener('DOMContentLoaded', () => {
    flatpickr("#start-date", {
      dateFormat: "Y-m-d"
    });
  
    flatpickr("#end-date", {
      dateFormat: "Y-m-d"
    });

    const clearButtons = document.querySelectorAll('.clear-button');
    
    clearButtons.forEach(button => {
      button.addEventListener('click', () => {
        const targetInput = document.querySelector(button.getAttribute('data-target'));
        if (targetInput) {
          targetInput.value = '';
          targetInput._flatpickr.clear();
        }
      });
    });
  });
  
